# Sequence Diagram Example

Repository untuk kebutuhan lampiran tugas Studi Literatur mata kuliah DAA.

![*Sequence diagram* dari *source code*, dokumen pribadi](https://www.plantuml.com/plantuml/png/nLPTJuCm57tVhxZP8ud6iJSobqrzDCRC-6mQk9bD-B8KTF_UqeuEQ2aGDV8mjikvdptdZbt12UDb4aEEhCZIa9NH8XSc6jASf1mIGjEEeK1MqH1Xiu5dwN9_U7ruEHktsmvMnJnWdXMKP-nKE-qP8wT7Ml2DSj_MJekhhSf4Gauhmb7z0d7LNq0yRl9wdZHhuDakZVNLu8IBaTxp880KFyqYjBlXqqeT07-dnNKVOaijgmuwnKLer2pjSPRbiBhXDC52X-ft7K38KBXgm63_iWdZ3DvMbmEh1zv66nCJO3qfvTB1wzGDklFqCaGHfXtoFBsK90YtKDzc-P5ZxIO-dLwNUULcuQqJi9Rkeau098fSOx3mmPfp06TofzV047raAJ47h305caRurObh4gdCepQHHiaqFfaZSiDZMjSEmlh3X7LfLNjB1jigQr76jpKqEPMJQtFEtYv2N-uF_cGJIf_7_iGkPL0jm_62BO472zqV_w9Ec-1dbjdHKmzgKLNp25Ebz_e-Pr4pv8mADKvRrVEKPT79UbWdqzddKD9QAMc0uXeqHKWE4VtFpbHmzPU47oM9ONbNHdGvia4ZZF5tDoZ39Alm1qjqwiYcyhs5aszYYvqmYzV4Rm00 "personcrud")

import java.util.ArrayList;

public class Example {

   public static void main(String args[]){
      PersonCRUDService service = new PersonCRUDService();

      for (int i = 0; i <= 10; i++) {
         service.create("Budi", 20);
         if (i % 2 == 0) {
            service.update(i, "Andi", 10);
         }
      }

      for (int i = 0; i <= 10; i++) {
         Person person = service.retrieve(i);
         if (person.getName().equals("Budi")) {
            service.delete(i);
         }
      }
   }
}

class Person {

   private String name;
   private int age;

   public Person(String name, int age) {
      this.name = name;
      this.age = age;
   }

   public String getName() { return this.name; }
   public int getAge() { return this.age }
   public void setName(String name) { this.name = name; }
   public void setAge(int age) { this.age = age; }

}

class PersonCRUDService {

   private ArrayList<Person> personRepository;

   public PersonCRUDService() {
      this.personRepository = new ArrayList<Person>();
   }

   public Person create(String name, int age) {
      Person person = new Person(name, age);
      this.personRepository.add(person);
      return person;
   }

   public Person retrieve(int id) {
      return this.personRepository.get(id);
   }

   public Person update(int id, String name, int age) {
      Person person = this.personRepository.get(id);
      person.setName(name);
      person.setAge(age);
      return person;
   }

   public void delete(int id) {
      this.personRepository.remove(id);
   }

}
